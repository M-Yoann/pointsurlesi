-- mapping
function map(mode, shortcut, command)
  vim.api.nvim_set_keymap(mode, shortcut, command, { noremap = true, silent = true })
end

function nmap(shortcut, command)
  map('n', shortcut, command)
end

function vmap(shortcut, command)
  map('v', shortcut, command)
end

function imap(shortcut, command)
  map('i', shortcut, command)
end

function novmap(shortcut, command)
  map('n', shortcut, command)
  map('o', shortcut, command)
  map('v', shortcut, command)
end

-- windows moves
nmap('<C-j>',       '<C-W><C-j>')
nmap('<C-Down>',    '<C-W><C-j>')
nmap('<C-k>',       '<C-W><C-k>')
nmap('<C-Up>',      '<C-W><C-k>')
nmap('<C-h>',       '<C-W><C-h>')
nmap('<C-Left>',    '<C-W><C-h>')
nmap('<C-l>',       '<C-W><C-l>')
nmap('<C-Right>',   '<C-W><C-l>')

-- file finding
nmap('<C-p>', "<cmd>Telescope find_files<cr>")
nmap('<C-o>', "<cmd>Telescope live_grep<cr>")
nmap(';'    , "<cmd>Telescope buffers<cr>")
nmap('<C-i>', "<cmd>NERDTreeToggle<cr>")
nmap('ç'    , "<cmd>NERDTreeFind<cr>")

-- copy
nmap('y', '"+y')
vmap('y', '"+y')

vmap('<leader>c', 'c<c-r>=system("base64 -w 0", @")<cr>')
vmap('<leader>d', 'c<c-r>=system("base64 -d", @")<cr>')

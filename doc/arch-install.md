# Install

General install : https://wiki.archlinux.org/title/Installation_guide
LVM on LUKS: https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS

## Load keymapping
`loadkeys fr-latin9`
Can be listed with:
`ls /usr/share/kdb/keymaps/**/*.map.gz`


## Check uefi:
`cat /sys/firmware/efi/fw_platform_size` should  return "64"

## Check network:
`ping archlinux.org`

## check clock:
`timedatectl`: should return correct time

## Disk partition

### See disks:
`lsblk`

### create partitions:
`gdisk /path/drive`. Ex : `gdisk /dev/sda`

We create a small partition efi and all the rest for lvm
`o` then `y`: to create table
`n` then `size` to create partition
Ex: For 512M efi and what is left in lvm
`n`, Default, Default, `+512MiB`, `ef00`
`n`, Default, Default, Default, `8e00`

Once partition are created : `w`

### Create file system on each partitions
`mkfs.filesystemName /dev/...letter`
Ex: `mkfs.ext4 /dev/sda1, mkfs.fat -F32 /dev/sda2, mkswap /dev/sda3` 

In this exemple, only efi partition need to be formated
Ex: `mkfs.fat -F32 /dev/sda1`
For lvm, the partition must be formated with cryptsetup (not mkfs)

### luks

To crypt a partition: `cryptsetup luksFormat /dev/....`
To open it `cryptsetup open --type luks /dev/s... <name_you_want>` (`name_you_want` will be `lvm` in this doc)
A file `/dev/mapper/lvm` will be created

#### create volume group
`pvcreate /dev/mapper/lvm`
`vgcreate <name_you_want> /dev/mapper/lvm` (`name_you_want` will be `volume` in this doc)

#### create luks partitions
`lvcreate -L<size> volume -n <label>`
size look like this `-L2G` but equal to `-l 100%FREE` if last partition
label are the name you want.
Exemple, with swap and root:
```
lvcreate -L2G volume -n swap
lvcreate -l 100%FREE volume -n root
```

#### Create file system on each luks partitions
`mkfs.filesystemName /dev/mapper/volume-<label>`
Ex:
```
mkfs.ext4 /dev/mapper/volume-root
mkswap /dev/mapper/volume-swap
```

## mount filesystem
`mount /partition/path /dest`

Ex:
```
mount /dev/mapper/volume-root /mnt
mount --mkdir /dev/sda1 /mnt/boot
swapon /dev/mapper/volume-swap
```

## boot strap:
`pacstrap -K /mnt programs`
programs are base, linux and linux-firmware (base-devel optionnal)
Ex: `pacstrap -K /mnt base linux linux-firmware`

## fstab
Create fstab file
`genfstab -U /mnt >> /mnt/etc/fstab`

## Chroot
Chroot allow us to "be" is a different root folder. Every command will consider this folder as the root folder
`arch-chroot /mnt`

### vim
Install vim: `pacman -S vim`

### Time
Set time zone:
`ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime`

`ls /use/share/zoneinfo` to check options

Synchronize : `hwclock --systohc`

### locals
Edit /etc/locale.gen
Uncomment the two lines `en_US`
Run `locale-gen`
Run `locale > /etc/locale.conf`
Set keyboard layout in /etc/vconsole.conf : 
`KEYMAP=fr-latin9`

### Network

install networkmanager: `pacman -S networkmanager`
enable networkmanager:  `systemctl enable NetworkManager`

create /etc/hostname and put the name you want
create /etc/hosts
```
127.0.0.1 localhost
127.0.0.1 <hostname>
```

### Set root password
`passwd`

### Finish luks install

Edit /etc/mkinitcpio.conf, update HOOKS with information found [here](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_mkinitcpio_3). (use the first one)
Run `mkinitcpio -P`

### Install bootloader
We use systemd-boot
Run `bootctl install`
Edit /boot/loader/loader.conf
```
default arch
timeout 3
editor 0
```

Edit /boot/loader/entries/arch.conf
```
title Arch Linux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options cryptdevice=UUID=<uuid>:volume root=/dev/mapper/volume-root quiet rw
```
to get uuid inside vim, run `:read ! blkid <encrypted_partion_path>`
In our case `encrypted_partion_path` = `/dev/sda2`

## Unmount 
exit chroot : `exit`
Run `umount -R /mnt`
`reboot`


# Post install

## Add user
`useradd -m -g wheel yoann`

Uncomment `%wheel ALL=(ALL:ALL) ALL` in /etc/sudoers
Change sudo lock attempts number : `sudo vim /etc/security/faillock.conf` > deny 30

## Auto-login

Follow `https://wiki.archlinux.org/title/Getty > 2.3.1 Virtual console` to auto-login

## Xorg
- pacman -S i3 xorg-server xorg-xinit
- create /etc/X11/xorg.conf.d/20-keyboard.conf
    - Add this before starting xorg-server 
    ```
    Section "InputClass"
    	Identifier "keyboard"
    	MatchIsKeyboard "yes"
    	Option "XkbLayout" "fr"
    	Option "XkbVariant" "azerty"
    EndSection
    ```

## Programs
- pulseaudio, pulseaudio-alsa, pavucontrol, pulseaudio-bluetooth
- rofi, rofi-emoji, xdotool, xsel (pour vim aussi), papirus-icon-theme
- stow
- sysstat (to read cpu temperature)
- base-devel
- autorandr
- firefox
- git
- alacritty
- bash-completion
- fuse2 (for AppImages)
- ristretto (image viewer)
- vlc
- picom (compositor)
- alsa-utils (for i3block/pavucontrol)
- mate-calc
- screengrab
- xorg-xev (touche/code clavier)
- neovim (appimage), ripgrep
- usb_modeswitch # to activate internet through usb
- pacman-contrib
- acpi : pour le status de la batterie
- xf86-input-synaptics: suivre https://wiki.archlinux.org/title/Touchpad_Synaptics

### Thunar
- Install: thunar, thunar-volman, gvfs, udisks2, polkit, lxqt-policykit, ntfs-3g, gnome-keyring (option: seahorse (gui password manager))
- Configure polkit to always allow :
Create /etc/polkit-1/rules.d/50-udiskie.rules
```
polkit.addRule(function(action, subject) {
  var YES = polkit.Result.YES;
  var permission = {
    // required for udisks1:
    "org.freedesktop.udisks.filesystem-mount": YES,
    "org.freedesktop.udisks.luks-unlock": YES,
    "org.freedesktop.udisks.drive-eject": YES,
    "org.freedesktop.udisks.drive-detach": YES,
    // required for udisks2:
    "org.freedesktop.udisks2.filesystem-mount": YES,
    "org.freedesktop.udisks2.encrypted-unlock": YES,
    "org.freedesktop.udisks2.eject-media": YES,
    "org.freedesktop.udisks2.power-off-drive": YES,
    // required for udisks2 if using udiskie from another seat (e.g. systemd):
    "org.freedesktop.udisks2.filesystem-mount-other-seat": YES,
    "org.freedesktop.udisks2.filesystem-unmount-others": YES,
    "org.freedesktop.udisks2.encrypted-unlock-other-seat": YES,
    "org.freedesktop.udisks2.encrypted-unlock-system": YES,
    "org.freedesktop.udisks2.eject-media-other-seat": YES,
    "org.freedesktop.udisks2.power-off-drive-other-seat": YES
  };
  if (subject.isInGroup("storage")) {
    return permission[action.id];
  }
});
```
and add self to storage group : `sudo usermod -a -G storage <user>`


and use a blank password

## fonts
- ttf-noto-nerd
- ttf-dejavu
- ttf-firacode-nerd

## Gtk
- Install lxappearance
- Search themes
`sudo pacman -Ss "gtk-theme$"` and install
- Search cursor
`sudo pacman -Ss "^xcursor-"` and install


# config
- systemctl enable --user pulseaudio.service
- sync clock through network : `timedatectl set-ntp true`

# install yay
- install go
- clone repo
- makepkg
- sudo pacam -U nom-généré-*.tar.zst

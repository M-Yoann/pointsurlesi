#!/bin/sh

# In order for this script to work when called by transmission, you need:
#  - To install transmission-cli
#  - Allow remote access


#Variables accessibles au lancement du script par transmission
#TR_APP_VERSION
#TR_TIME_LOCALTIME
#TR_TORRENT_DIR
#TR_TORRENT_HASH
#TR_TORRENT_ID
#TR_TORRENT_NAME

# port, username, password
#SERVER="9091 --auth transmission:transmission"
SERVER=""
transmission-remote $SERVER --torrent $TR_TORRENT_ID --remove;


#!/bin/bash

locked_screen_folder=~/.pointsurlesi/images/locked_screens/
locked_screens=(`ls $locked_screen_folder | grep "jpg\|png"`)
today_locked_screen=${locked_screens[$(( 10#$(date +%j) % ${#locked_screens[@]} ))]}
i3lock -ti $locked_screen_folder$today_locked_screen

#!/bin/bash

wallpaperFolder=~/.pointsurlesi/images/wallpapers/
wallpapers=(`ls $wallpaperFolder | grep "jpg\|png"`)
todayWallpaper=${wallpapers[$(( 10#$(date +%j) % ${#wallpapers[@]} ))]}
feh --bg-scale $wallpaperFolder$todayWallpaper

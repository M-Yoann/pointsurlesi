#!/bin/bash

# Ajouter une variable d'environement ?

home_dir='/home/yoann'
disk='/run/media/yoann/211f0b96-ee50-4d4e-8974-11138c733a34'
remote='ovh'
config=$home_dir'/.config/rclone/rclone.conf'
log_file=$home_dir'/.rclone.log'

folders_home=(
Documents
)
folders_disk=(
Pictures
)

sleep 300
notify-send -a "Rclone-Backup" " Started"
# Backup external drive
if [ -d $disk ]
then
    for path in ${folders_disk[@]}
    do
        rclone sync $disk/$path $remote:/disk/$path --bwlimit 200k --config=$config --log-file=$log_file
    done
fi


# Backup home
for path in ${folders_home[@]}
do
    rclone sync $home_dir/$path $remote:/home/$path --bwlimit 200k --config=$config --log-file=$log_file
done

notify-send -a "Rclone-Backup" " Finished"
exit

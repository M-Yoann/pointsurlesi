# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

#ls
alias ll='ls -alFh'
alias la='ll -A'
alias l='ls -CF'

#pacman
alias pacin='sudo pacman -S '
alias pacup='sudo pacman -Syu'

#docker
alias dc='docker compose'
alias dcu='docker compose up -d'
alias dcd='docker compose down'
alias dce='docker compose exec'
alias dcr='docker compose run'

#vim
alias vim='nvim'

# kube
alias k="kubectl"

set encoding=utf-8  " The encoding displayed.
set fileencoding=utf-8  " The encoding written to file.
" Screen/tmux can also handle xterm mousiness, but Vim doesn't
" detect it by default.
"mouse
if &term == "screen"
set ttymouse=xterm2
endif
if v:version >= 704 && &term =~ "^screen"
set ttymouse=sgr
endif
set mouse=a

set scrolloff=5             " Keep cursor off edge by 5

" Lines
set ic hls is
set number relativenumber
set signcolumn=number       " merge vim-gutter with number column

" Others
set nocompatible            " Disable compatibility to old-time vi
set noswapfile              " No swap files
set showmatch               " Show matching brackets.
set tabstop=4               " number of columns occupied by a tab character
set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing
set shiftwidth=4            " width for autoindents
set autoindent              " indent a new line the same amount as the line just typed
set expandtab               " converts tabs to white space
set autowrite               " automaticly write change when leaving a buffer
set autoread                " automaticly reload file if other program (eg:git) changes it

"set tw=90
" set cuc
set cul
syntax on

" Color
if $TERM == "rxvt-unicode"
  set t_Co=256
endif

" Fix escape character:
map <ESC>[1;5D <C-Left>
map <ESC>[1;5C <C-Right>
map! <ESC>[1;5D <C-Left>
map! <ESC>[1;5C <C-Right>

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" Vim-plug
call plug#begin('~/.vim/plugged')
" cosmetics
"Plug 'jsit/toast.vim'
"Plug 'joshdick/onedark.vim'
Plug 'chriskempson/vim-tomorrow-theme'
Plug 'w0ng/vim-hybrid'
Plug 'sainnhe/edge'
Plug 'vim-airline/vim-airline'          " Purely asthetique: see buffers, color mode (insert, visual)
Plug 'vim-airline/vim-airline-themes'   " Handle preconfigure themes for vim-ailine
Plug 'wincent/terminus'                 " cursor change appearance depending of mode (also tmux compatibility)
Plug 'ryanoasis/vim-devicons'

" utilities
Plug 'preservim/nerdtree'              " :NERDTree to open
Plug 'junegunn/fzf' , { 'do': { -> fzf#install() }} " :Files : find files :Buffers
Plug 'junegunn/fzf.vim'

" Parse/modify general code
Plug 'tpope/vim-commentary'             " gcc to comment, gc to comment block
Plug 'ntpeters/vim-better-whitespace'   " highlight whitespaces & strip them (StripWhitespace)

"Plug 'yuttie/comfortable-motion.vim'    " pretty scroll
Plug 'moll/vim-bbye'                   " close buffer without closing windows
Plug 'tpope/vim-fugitive'              " git tool: allow git command
"Plug 'tpope/vim-rhubarb'               " github wrapper
"Plug 'airblade/vim-gitgutter'          " show git status in sign

Plug 'leafgarland/typescript-vim'

" Code
Plug 'dense-analysis/ale'
" Debbuger
"Plug 'vim-vdebug/vdebug', { 'tag': 'a8c2e295248bdf1106f07667be544331fe4ee8fc' }   " multi languages debug server
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

"PHP
"Plug 'StanAngeloff/php.vim'           " php syntax highlight
"Plug 'phpactor/phpactor', {'for': 'php', 'do': 'composer install --no-dev -o'}
"Plug '2072/PHP-Indenting-for-VIm'
Plug 'stephpy/vim-php-cs-fixer'
"Plug 'vim-scripts/phpfolding.vim'
call plug#end()

" cosmetics
set background=dark
colorscheme hybrid
"set background=dark
"colorscheme desert

" Go
let g:go_debug_windows = { 'vars': 'rightbelow 60vnew', 'stack': 'rightbelow 10new'}

" Powerline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline_theme='deus'

" Netrw (Tree)
let g:netrw_liststyle = 3 "Style of the explorer
let g:netrw_banner = 0 "Remove the banner inside explorer
let g:netrw_browse_split = 4 "open in previous windows
let g:netrw_winsize = 25 "size of explorer (%)
let g:netrw_altv = 1

"Lexplore toogle netrw verticaly

" Split :sp or :vs
" :Ag full search
" Smart way to move between windows
map <C-j> <C-W><C-j>
map <C-k> <C-W><C-k>
map <C-h> <C-W><C-h>
map <C-l> <C-W><C-l>

map <C-a> :set number! relativenumber!<CR>
map <C-p> :GFiles<CR>
map <C-o> :Ag<CR>
"map <C-i> :Lexplore<CR>
map <C-i> :NERDTreeToggle<CR>
map ; :Buffers<CR>
map ² :NERDTreeFind<CR>
map <F12> :ALEGoToDefinition<CR>
map <S-F12> :ALEFindReferences<CR>
map <F9> :ALERename<CR>
"%!jq .             " command to reformat json

let g:ale_fixers = {'*': ['remove_trailing_lines', 'trim_whitespace'], 'javascript': ['eslint'], 'typescript': ['eslint']}
"hi CursorLine gui=NONE guifg=NONE guibg=#3a3a3a cterm=NONE ctermfg=NONE ctermbg=237
"hi CursorColumn gui=NONE guifg=NONE guibg=#3a3a3a cterm=NONE ctermfg=NONE ctermbg=237

let g:vdebug_options = { 'port' : 9000,"break_on_open" : 0, 'path_maps': {'/var/www/v3': '/home/yoann/dev/V3'}, 'continuous_mode' : 1}

"YML auto-indent
"au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
"autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

let g:php_cs_fixer_level = "symfony"                   " options: --level (default:symfony)
let g:php_cs_fixer_config = "default"                  " options: --config
let g:php_cs_fixer_rules = "@PSR2"          " options: --rules (default:@PSR2)
let g:php_cs_fixer_php_path = "/urs/bin/php"               " Path to PHP
let g:php_cs_fixer_enable_default_mapping = 0     " Enable the mapping by default (<leader>pcd)
let g:php_cs_fixer_dry_run = 0                    " Call command with dry-run option
let g:php_cs_fixer_verbose = 0
